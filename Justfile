
master := "servie.home.iruud.cloud"
op_connect_host := "https://onepassword.service.ruud.cloud"

# How to check that these exist?
root_ca_path := "modules/profile/files/ruud_cloud_root_ca.crt"
root_key_path := "key_root.pem.gpg"

export GPG_PUB_ARMOR := `gpg --export --armor post@barskern.no`
export VAULT_KEY := `cat vault_key.gpg | base64 -d | gpg -dq`
export VAULT_ROOT_TOKEN := `cat vault_root_token.gpg | base64 -d | gpg -dq`
export VAULT_TOKEN := if path_exists(".bolt-vault-token") == "true" {
	`cat .bolt-vault-token`
} else {
	VAULT_ROOT_TOKEN
}
export VAULT_ORUUD_PASSWORD := `pass show oruud-vault`
export OP_CONNECT_TOKEN := `cat vault_op_token.gpg | base64 -d | gpg -dq`
export OP_CREDENTIALS := `cat 1password-credentials.json.gpg | gpg -dq`

apply:
	bolt apply -t {{master}} manifests/site.pp -v

router:
	bolt plan run control::setup_router -t routers -v

update:
	bolt module install --force

vault-unseal:
	bolt task run control::unseal_vault -t {{master}} key=${VAULT_KEY}

vault-regen-certs:
	bolt task run control::regen_vault_certs -t {{master}} token=${VAULT_TOKEN}

vault-setup-database:
	vault secrets enable \
		-description="database credentials" \
		database

vault-setup-secrets:
	vault secrets enable \
		-version=2 \
		-path=secret \
		-description="key-value secrets" \
		kv

vault-setup-userpass:
	bolt task run control::setup_vault_admin -t {{master}} \
			token=${VAULT_ROOT_TOKEN} \
			gpg_key="${GPG_PUB_ARMOR}" \
			password=${VAULT_ORUUD_PASSWORD}

vault-setup-op:
	vault secrets disable op
	vault plugin register \
		-sha256=8eb865ca4ac9c7c87fa902985383da0132462f299765752f74e6f212e796a5bd \
		-command=vault-plugin-secrets-onepassword_v1.1.0 \
		-version=v1.1.0 \
		secret \
		op-connect
	vault secrets enable \
	  -plugin-name=op-connect \
	  -path=op \
	  -description="read and write to onepassword vaults" \
	  plugin
	vault write op/config \
		op_connect_host={{op_connect_host}} \
		op_connect_token=${OP_CONNECT_TOKEN}

vault-setup-op-cred:
	#!/bin/bash
	set -eou pipefail
	echo ${OP_CREDENTIALS} | vault kv put secret/op_connect/credentials -
	vault kv get secret/op_connect/credentials

nomad-regen-token:
	bolt task run control::update_nomad_vault_token -t {{master}} token=${VAULT_TOKEN}

vault-import-root-ca:
	vault secrets enable \
		-description="root certificate authority" \
		-path=pki_root \
		pki
	vault secrets tune -max-lease-ttl="175200h" pki_root
	gpg -q -d {{root_key_path}} \
		| cat {{root_ca_path}} <(echo '') - \
		| vault write pki_root/issuers/import/bundle pem_bundle=-
