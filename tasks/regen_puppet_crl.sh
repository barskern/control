#!/usr/bin/env bash

set -e

#exec 3>&1
#exec 1>&2
exec 2>&1

#export VAULT_TOKEN=$PT_token
#export VAULT_ADDR="http://127.0.0.1:8200" # the script should run on master vault instance AFTER port change
#export VAULT_ADDR="https://vault.service.ruud.cloud:8200"
export VAULT_ADDR=$PT_vault_addr

tmp_wd=$(mktemp -d -t regen-puppet-crl-XXXXXXXX)
echo "Using temporary directory '$tmp_wd'"
cd $tmp_wd

cleanup () {
	rv="$?"
	if [ "$rv" -ne 0 ]
	then
		echo "Exited due to error ($rv)"
	fi

	cd /
	rm -rf $tmp_wd
}

# Delete temporary directory on exit (no matter if error or not)
trap 'cleanup' EXIT

echo "Fetch CRLs from Vault..."
cat <(vault read -field=certificate pki_puppet/cert/crl) \
	<(echo) \
	<(vault read -field=certificate pki_infra/cert/crl) \
	<(echo) \
	<(vault read -field=certificate pki_root/cert/crl) \
	<(echo) > crls.pem

echo "Send CRLs to puppet..."
curl \
	--silent \
	--write-out " (status_code=%{http_code})\n" \
	--data-binary @crls.pem \
	--request PUT \
	--cert $(puppet config print hostcert) \
	--key $(puppet config print hostprivkey) \
	--cacert $(puppet config print cacert) \
	https://servie.home.iruud.cloud:8140/puppet-ca/v1/certificate_revocation_list
