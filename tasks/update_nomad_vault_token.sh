#!/usr/bin/env bash

set -e

#exec 3>&1
#exec 1>&2
exec 2>&1

export VAULT_TOKEN=$PT_token
export VAULT_ADDR="http://127.0.0.1:8200"

force=$PT_force
filepath="/etc/vault-agent/token-nomad-env"
#filepath="/etc/nomad.d/nomad.env"

if [ -f "$filepath" ]; then
	prev_token=$(cat "$filepath" | grep '^VAULT_TOKEN=\S\+$' | sed "s/^VAULT_TOKEN=\\(.\\+\\)$/\\1/" | tr -d ' \n')

	if [ -n "$prev_token" ]; then
		if VAULT_TOKEN=$prev_token vault token lookup >/dev/null 2>&1; then
			if [ "$force" = "true" ]; then
				echo "Previous token still valid, but overwriting due to force..."
			else
			   echo "Previous token is still valid. Pass 'force=true' to overwrite it."
			   exit 0
			fi
		fi
	fi
fi

new_token=$(vault token create \
	-field=token \
	-policy=base \
	-policy=nomad_server \
	-metadata="created_by=$(whoami)@$(hostname) through bolt script" \
	-period=72h \
	-orphan \
)
if [ -f "$filepath" ]; then
	sed -i "/^VAULT_TOKEN=.\\+$/d" $filepath
fi

echo -n "VAULT_TOKEN=$new_token" >> $filepath
pkill -HUP nomad || true

echo "Successfully triggered reload of token for nomad"
