#!/usr/bin/env bash

set -euo pipefail

exec 3>&1
exec 1>&2

BASE_NAME="Ruud's Homelab"
COUNTRY="NO"

export VAULT_TOKEN=$PT_token
export VAULT_ADDR="$PT_vault_addr"

BASE_CERT_ADDR="$PT_cert_addr"
GPG_KEY="$PT_gpg_key"

SKIP_ROOT="$PT_skip_root"

tmp_wd=$(mktemp -d -t setup-vault-ca-XXXXXXXX)
echo "Using temporary directory '$tmp_wd'"
cd $tmp_wd

cleanup_vault () {
	[[ "$SKIP_ROOT" != "true" ]] && vault secrets disable pki_root || true
	vault secrets disable pki_infra || true
	vault secrets disable pki_puppet || true
	vault secrets disable pki_web || true

	vault auth disable cert || true
}

cleanup () {
	rv="$?"
	if [ "$rv" -ne 0 ]
	then
		echo "Exited due to error ($rv), cleaning up vault resources..."
		cleanup_vault
	fi

	cd /
	rm -rf $tmp_wd
}

# Delete temporary directory on exit (no matter if error or not)
trap 'cleanup' EXIT

# Add gpg key to temporary homedir to enable encryption
# See: https://security.stackexchange.com/a/86751
mkdir 700 gnupg
gpg --homedir gnupg --import <(echo "$GPG_KEY")
KEYID="$(gpg --list-public-keys --batch --with-colons --homedir gnupg | grep "^pub" | cut -d: -f5)"

encrypt() {
	gpg --homedir gnupg --encrypt --recipient "$KEYID" --trust-model always --armor
}

#
# Ensure there are no existing secret nor auth measures in vault
#
cleanup_vault


#
# Setup root CA
#
echo "Setting up root CA..."
if [[ "$SKIP_ROOT" != "true" ]]; then
	vault secrets enable \
	  -description="root certificate authority" \
	  -path=pki_root \
	  pki
	vault secrets tune -max-lease-ttl="175200h" pki_root
	encrypted_key_root="$(\
		vault write -field=private_key pki_root/root/generate/exported \
			common_name="$BASE_NAME CA" \
			ttl="175200h" \
			key_type="ec" \
			key_bits="384" \
			exclude_cn_from_sans="true" \
			country="$COUNTRY" \
		| encrypt \
	)"
else
	encrypted_key_root=""
fi
vault read -field=certificate pki_root/cert/ca > ca.crt
# See https://www.vaultproject.io/api-docs/secret/pki#set-revocation-configuration
vault write pki_root/config/crl \
	auto_rebuild=true
vault write pki_root/config/urls \
	issuing_certificates="$BASE_CERT_ADDR/v1/pki_root/ca" \
	crl_distribution_points="$BASE_CERT_ADDR/v1/pki_root/crl"
vault write pki_root/config/auto-tidy \
	enabled=true \
	tidy_cert_store=true \
	tidy_revoked_certs=true \
	tidy_expired_issuers=true


#
# Setup intermediate CA for infrastructure
#

echo "Setting up intermediate CAs for infrastructure..."
vault secrets enable \
  -description="intermediate certificate authority for infrastructure" \
  -path=pki_infra \
  pki
vault secrets tune -max-lease-ttl="17088h" pki_infra
encrypted_key_infra="$(\
	vault write -format=json pki_infra/intermediate/generate/exported \
		common_name="$BASE_NAME Infrastructure CA" \
		ttl="17088h" \
		key_type="ec" \
		key_bits="384" \
		exclude_cn_from_sans="true" \
		country="$COUNTRY" \
	| tee >(jq -r .data.csr > csr_infra.csr) \
	| jq -r .data.private_key \
	| encrypt \
)"
vault write -field=certificate pki_root/root/sign-intermediate \
	csr=@csr_infra.csr \
	ttl="17088h" \
	use_csr_vaules="true" > ca_infra.crt
# TODO is chain needed, guide says only cert should be imported?
#cat ca_infra.crt \
#	<(echo) \
#	<(vault read -field=certificate pki_root/cert/ca) > ca_infra_chain.crt
vault write pki_infra/intermediate/set-signed \
	certificate=@ca_infra.crt
# See https://www.vaultproject.io/api-docs/secret/pki#set-revocation-configuration
vault write pki_infra/config/crl \
	auto_rebuild=true
vault write pki_infra/config/urls \
	issuing_certificates="$BASE_CERT_ADDR/v1/pki_infra/ca" \
	crl_distribution_points="$BASE_CERT_ADDR/v1/pki_infra/crl"
vault write pki_infra/roles/iruud_cloud \
	allowed_domains="iruud.cloud" \
	allow_subdomains="true" \
	client_flag="false"
vault write pki_infra/config/auto-tidy \
	enabled=true \
	tidy_cert_store=true \
	tidy_revoked_certs=true \
	tidy_expired_issuers=true

#
# Setup intermediate CA for puppet
#
# NB! Puppet does not seem to support elliptic curve, so use rsa.
#

echo "Setting up intermediate CAs for puppet..."
vault secrets enable \
  -description="intermediate certificate authority for puppet nodes" \
  -path=pki_puppet \
  pki
vault secrets tune -max-lease-ttl="17088h" pki_puppet
key_puppet="$(\
	vault write -format=json pki_puppet/intermediate/generate/exported \
		common_name="$BASE_NAME Puppet CA" \
		ttl="17088h" \
		key_type="rsa" \
		exclude_cn_from_sans="true" \
		country="$COUNTRY" \
	| tee >(jq -r .data.csr > csr_puppet.csr) \
	| jq -r .data.private_key \
)"
vault write -field=certificate pki_infra/root/sign-intermediate \
	csr=@csr_puppet.csr \
	ttl="17088h" \
	use_csr_vaules="true" > ca_puppet.crt
# TODO see comment above regarding chains for certs..
#cat ca_puppet.crt \
#	<(echo) \
#	<(vault read -field=certificate pki_infra/cert/ca) \
#	<(echo) \
#	<(vault read -field=certificate pki_root/cert/ca) > ca_puppet_chain.crt
vault write pki_puppet/intermediate/set-signed \
	certificate=@ca_puppet.crt
# See https://www.vaultproject.io/api-docs/secret/pki#set-revocation-configuration
vault write pki_puppet/config/crl \
	auto_rebuild=true
vault write pki_puppet/config/urls \
	issuing_certificates="$BASE_CERT_ADDR/v1/pki_puppet/ca" \
	crl_distribution_points="$BASE_CERT_ADDR/v1/pki_puppet/crl"
vault write pki_puppet/config/auto-tidy \
	enabled=true \
	tidy_cert_store=true \
	tidy_revoked_certs=true \
	tidy_expired_issuers=true

#
# Setup puppet for External Intermediate CA
#
# See https://puppet.com/docs/puppetserver/latest/intermediate_ca.html
#

cat ca_puppet.crt <(echo) ca_infra.crt <(echo) ca.crt > ca-bundle.pem
cat <(vault read -field=certificate pki_puppet/cert/crl) \
	<(echo) \
	<(vault read -field=certificate pki_infra/cert/crl) \
	<(echo) \
	<(vault read -field=certificate pki_root/cert/crl) > crls.pem

puppet=/opt/puppetlabs/bin/puppet
puppetserver=/opt/puppetlabs/bin/puppetserver

# NB! Clean out previous ssl certificates to import new ones, but make sure
# puppetserver is stopped! Hence we store existing status to reenable (if
# applicable) after resetting ssl certificates.
puppetserver_ensure=$(\
	$puppet resource service puppetserver --to-yaml \
		| grep ensure \
		| cut -d : -f 2 \
		| sed 's/^\s*//;s/\s*$//' \
	)
puppet_ensure=$(\
	$puppet resource service puppet --to-yaml \
		| grep ensure \
		| cut -d : -f 2 \
		| sed 's/^\s*//' \
	)
$puppet resource service puppet ensure=stopped
$puppet resource service puppetserver ensure=stopped

rm -fr /etc/puppetlabs/puppet/ssl/*
rm -fr /etc/puppetlabs/puppetserver/ca/*
$puppetserver ca import \
	--cert-bundle ca-bundle.pem \
	--crl-chain crls.pem \
	--private-key <(echo "$key_puppet")

$puppet resource service puppetserver ensure="$puppetserver_ensure"
$puppet resource service puppet ensure="$puppet_ensure"

#
# Enable certificates from infra and puppet as auth for vault
#

echo "Enabling certificates from infra and puppet as auth for vault..."
vault auth enable \
	-description="authorize using certificates from infrastructure" \
	cert
vault write auth/cert/certs/infra \
	certificate=@ca_infra.crt \
	display_name="$BASE_NAME Infra Token" \
	allowed_common_names="*.iruud.cloud" \
	token_policies="infra_node" \
	ttl=3600
vault write auth/cert/certs/puppet \
	certificate=@ca_puppet.crt \
	display_name="$BASE_NAME Puppet Token" \
	allowed_common_names="*.iruud.cloud" \
	token_policies="puppet_node" \
	ttl=3600

#
# Setup intermediate CA for website certificates
#

vault secrets enable \
  -description="intermediate certificate authority for web services" \
  -path=pki_web \
  pki
vault secrets tune -max-lease-ttl="17088h" pki_web
vault write -field=csr pki_web/intermediate/generate/internal \
	common_name="$BASE_NAME Web CA" \
	ttl="17088h" \
	key_type="ec" \
	key_bits="384" \
	exclude_cn_from_sans="true" \
	country="$COUNTRY" > csr_web.csr
vault write -field=certificate pki_root/root/sign-intermediate \
	csr=@csr_web.csr \
	ttl="17088h" \
	use_csr_vaules="true" > ca_web.crt
#cat ca_web.crt \
#	<(echo) \
#	<(vault read -field=certificate pki_root/cert/ca) > ca_web_chain.crt
vault write pki_web/intermediate/set-signed \
	certificate=@ca_web.crt
# See https://www.vaultproject.io/api-docs/secret/pki#set-revocation-configuration
vault write pki_web/config/crl \
	auto_rebuild=true
vault write pki_web/config/urls \
	issuing_certificates="$BASE_CERT_ADDR/v1/pki_web/ca" \
	crl_distribution_points="$BASE_CERT_ADDR/v1/pki_web/crl"
vault write pki_web/roles/ruud_cloud \
	allowed_domains="iruud.cloud,ruud.cloud" \
	allow_subdomains="true" \
	client_flag="false"
vault write pki_web/config/auto-tidy \
	enabled=true \
	tidy_cert_store=true \
	tidy_revoked_certs=true \
	tidy_expired_issuers=true


#
# Return encrypted private keys to user as structured output
#

cat >&3 <<EOF
{
"encrypted_key_root":$(echo "$encrypted_key_root" | jq -aRs .),
"encrypted_key_infra":$(echo "$encrypted_key_infra" | jq -aRs .)
}
EOF
