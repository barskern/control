#!/usr/bin/env bash

set -e

exec 3>&1
exec 1>&2

VAULT_KEY=$PT_key
export VAULT_ADDR="http://127.0.0.1:8200" # the script should run on master vault instance AFTER port change

if ! vault status >&2
then
	vault operator unseal $VAULT_KEY >&2
fi

vault status -format=json >&3
