#!/usr/bin/env bash

set -euo pipefail

exec 3>&1
exec 1>&2

export VAULT_TOKEN=$PT_token
export VAULT_ADDR="http://127.0.0.1:8200"

vault_dir=/etc/vault

ca_new_path=$vault_dir/ca.new.crt
cert_new_path=$vault_dir/cert.new.crt
key_new_path=$vault_dir/key.new.pem

ca_path=$vault_dir/ca.crt
cert_path=$vault_dir/cert.crt
key_path=$vault_dir/key.pem

#ip_sans=127.0.0.1,10.0.0.2,10.0.11.2 \

vault write -format json \
	pki_web/issue/ruud_cloud \
	common_name=vault.service.ruud.cloud \
	alt_names=servie.home.iruud.cloud \
	ttl=24h \
	| tee >(jq -r .data.issuing_ca > $ca_new_path) \
	| tee >(jq -r .data.certificate > $cert_new_path) \
	| tee >(jq -r '.data.ca_chain|join("\n")' >> $cert_new_path) \
	| tee >(jq -r .data.private_key > $key_new_path)

echo "Checking if new CA/cert/key are empty..."
[ -s $ca_new_path ] || { rm $vault_dir/*.new.{crt,pem}; exit 1; }
[ -s $cert_new_path ] || { rm $vault_dir/*.new.{crt,pem}; exit 1; }
[ -s $key_new_path ] || { rm $vault_dir/*.new.{crt,pem}; exit 1; }
echo "Success, moving certs and reloading vault!"

mv $ca_new_path $ca_path
mv $cert_new_path $cert_path
mv $key_new_path $key_path

chown vault:vault $ca_path
chown vault:vault $cert_path
chown vault:vault $key_path

chmod 644 $ca_path
chmod 644 $cert_path
chmod 600 $key_path

systemctl reload vault
