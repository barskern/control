#!/usr/bin/env bash

set -e

exec 3>&1
exec 1>&2

BASE_NAME="Ruud's Homelab"
COUNTRY="NO"

export VAULT_TOKEN=$PT_token
export VAULT_ADDR="$PT_vault_addr"

BASE_CERT_ADDR="$PT_cert_addr"
GPG_KEY="$PT_gpg_key"

tmp_wd=$(mktemp -d -t setup-vault-ca-XXXXXXXX)
echo "Using temporary directory '$tmp_wd'"
cd $tmp_wd

cleanup () {
	rv="$?"
	if [ "$rv" -ne 0 ]
	then
		echo "Exited due to error ($rv), cleaning up vault resources..."
	fi

	cd /
	rm -rf $tmp_wd
}

# Add gpg key to temporary homedir to enable encryption
# See: https://security.stackexchange.com/a/86751
mkdir gnupg
chmod 700 gnupg
gpg --homedir gnupg --import <(echo "$GPG_KEY")
KEYID="$(gpg --list-public-keys --batch --with-colons --homedir gnupg | grep "^pub" | cut -d: -f5)"

encrypt() {
	gpg --homedir gnupg --encrypt --recipient "$KEYID" --trust-model always --armor
}


# Read root CA, needed for later

vault read -field=certificate pki_root/cert/ca > ca.crt

#
# Setup intermediate CA for infrastructure
#

echo "Regen intermediate CAs for infrastructure..."
encrypted_key_infra="$(\
	vault write -format=json pki_infra/intermediate/generate/exported \
		common_name="$BASE_NAME Infrastructure CA" \
		ttl="17088h" \
		key_type="ec" \
		key_bits="384" \
		exclude_cn_from_sans="true" \
		country="$COUNTRY" \
	| tee >(jq -r .data.csr > csr_infra.csr) \
	| jq -r .data.private_key \
	| encrypt \
)"
vault write -field=certificate pki_root/root/sign-intermediate \
	csr=@csr_infra.csr \
	ttl="17088h" \
	use_csr_vaules="true" > ca_infra.crt
# TODO is chain needed, guide says only cert should be imported?
#cat ca_infra.crt \
#	<(echo) \
#	<(vault read -field=certificate pki_root/cert/ca) > ca_infra_chain.crt
vault write pki_infra/intermediate/set-signed \
	certificate=@ca_infra.crt

#
# Setup intermediate CA for puppet
#
# NB! Puppet does not seem to support elliptic curve, so use rsa.
#

echo "Regen intermediate CAs for puppet..."
key_puppet="$(\
	vault write -format=json pki_puppet/intermediate/generate/exported \
		common_name="$BASE_NAME Puppet CA" \
		ttl="17088h" \
		key_type="rsa" \
		exclude_cn_from_sans="true" \
		country="$COUNTRY" \
	| tee >(jq -r .data.csr > csr_puppet.csr) \
	| jq -r .data.private_key \
)"
vault write -field=certificate pki_infra/root/sign-intermediate \
	csr=@csr_puppet.csr \
	ttl="17088h" \
	use_csr_vaules="true" > ca_puppet.crt
# TODO see comment above regarding chains for certs..
#cat ca_puppet.crt \
#	<(echo) \
#	<(vault read -field=certificate pki_infra/cert/ca) \
#	<(echo) \
#	<(vault read -field=certificate pki_root/cert/ca) > ca_puppet_chain.crt
vault write pki_puppet/intermediate/set-signed \
	certificate=@ca_puppet.crt

#
# Setup puppet for External Intermediate CA
#
# See https://puppet.com/docs/puppetserver/latest/intermediate_ca.html
#

cat ca_puppet.crt <(echo) ca_infra.crt <(echo) ca.crt > ca-bundle.pem
cat <(vault read -field=certificate pki_puppet/cert/crl) \
	<(echo) \
	<(vault read -field=certificate pki_infra/cert/crl) \
	<(echo) \
	<(vault read -field=certificate pki_root/cert/crl) > crls.pem

# NB! Clean out previous ssl certificates to import new ones, but make sure
# puppetserver is stopped! Hence we store existing status to reenable (if
# applicable) after resetting ssl certificates.
puppetserver_ensure=$(\
	puppet resource service puppetserver --to-yaml \
		| grep ensure \
		| cut -d : -f 2 \
		| sed 's/^\s*//;s/\s*$//' \
	)
puppet_ensure=$(\
	puppet resource service puppet --to-yaml \
		| grep ensure \
		| cut -d : -f 2 \
		| sed 's/^\s*//' \
	)
puppet resource service puppet ensure=stopped
puppet resource service puppetserver ensure=stopped

rm -fr /etc/puppetlabs/puppet/ssl
rm -fr /etc/puppetlabs/puppetserver/ca
puppetserver ca import \
	--cert-bundle ca-bundle.pem \
	--crl-chain crls.pem \
	--private-key <(echo "$key_puppet")

puppet resource service puppetserver ensure="$puppetserver_ensure"
puppet resource service puppet ensure="$puppet_ensure"

#
# Enable certificates from infra and puppet as auth for vault
#

echo "Enabling certificates from new infra and puppet as auth for vault..."
vault write auth/cert/certs/infra \
	certificate=@ca_infra.crt \
	display_name="$BASE_NAME Infra Token" \
	allowed_common_names="*.iruud.cloud" \
	token_policies="infra_node" \
	ttl=3600
vault write auth/cert/certs/puppet \
	certificate=@ca_puppet.crt \
	display_name="$BASE_NAME Puppet Token" \
	allowed_common_names="*.iruud.cloud" \
	token_policies="puppet_node" \
	ttl=3600

#
# Setup intermediate CA for website certificates
#

echo "Regen intermediate CA for websites"
vault write -field=csr pki_web/intermediate/generate/internal \
	common_name="$BASE_NAME Web CA" \
	ttl="17088h" \
	key_type="ec" \
	key_bits="384" \
	exclude_cn_from_sans="true" \
	country="$COUNTRY" > csr_web.csr
vault write -field=certificate pki_root/root/sign-intermediate \
	csr=@csr_web.csr \
	ttl="17088h" \
	use_csr_vaules="true" > ca_web.crt
#cat ca_web.crt \
#	<(echo) \
#	<(vault read -field=certificate pki_root/cert/ca) > ca_web_chain.crt
vault write pki_web/intermediate/set-signed \
	certificate=@ca_web.crt

#
# Return encrypted private keys to user as structured output
#

cat >&3 <<EOF
{
"encrypted_key_infra":$(echo "$encrypted_key_infra" | jq -aRs .)
}
EOF
