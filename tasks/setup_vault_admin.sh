#!/usr/bin/env bash

set -eou pipefail

exec 3>&1
exec 1>&2

export VAULT_TOKEN=$PT_token
export VAULT_ADDR="$PT_vault_addr"

GPG_KEY="$PT_gpg_key"

ENTITY_NAME="$PT_entity_name"
USERNAME="$PT_username"
PASSWORD="$PT_password"

tmp_wd=$(mktemp -d -t setup-vault-ca-XXXXXXXX)
echo "Using temporary directory '$tmp_wd'"
cd $tmp_wd

cleanup_vault () {
	vault delete identity/entity/name/$ENTITY_NAME || true
	vault delete identity/group/name/admins || true

	vault auth disable userpass || true

	vault policy delete admin || true

	echo "Done cleaning up vault!"
}

cleanup () {
	rv="$?"
	if [ "$rv" -ne 0 ]
	then
		echo "Exited due to error ($rv), cleaning up vault resources..."
		cleanup_vault
	fi

	cd /
	rm -rf $tmp_wd
}

# Delete temporary directory on exit (no matter if error or not)
trap 'cleanup' EXIT

# Add gpg key to temporary homedir to enable encryption
# See: https://security.stackexchange.com/a/86751
mkdir gnupg
gpg --homedir gnupg --import <(echo "$GPG_KEY")
KEYID="$(gpg --list-public-keys --batch --with-colons --homedir gnupg | grep "^pub" | cut -d: -f5)"

encrypt() {
	gpg --homedir gnupg --encrypt --recipient "$KEYID" --trust-model always --armor
}

#
# Ensure there are no existing secret nor auth measures in vault
#
cleanup_vault

#
# Create basic admin policy
#
# NOTE! This policy is overridden with a more complex setup through the infra
# repository.
#
vault policy write admin -<<EOF
# Read system configuration
path "sys/*" {
  capabilities = ["read", "list", "sudo"]
}

# Create and manage policies
path "sys/policy/*" {
  capabilities = ["create", "read", "update", "delete", "sudo"]
}
path "sys/policies/*" {
  capabilities = ["list", "create", "read", "update", "delete"]
}

# Manage auth methods broadly across Vault
path "auth/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# Create, update, and delete auth methods
path "sys/auth/*" {
  capabilities = ["create", "update", "delete", "sudo"]
}

# Manage secrets engines
path "sys/mounts/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# Create and manage entities/groups
path "identity/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# Read access to all homelab secrets
path "op/vaults/homelab/*" {
  capabilities = ["read", "list"]
}
EOF

#
# Setup userpass auth and register admin user/group with identity/group policies
#

vault auth enable \
	-description="basic user and password authentication" \
	-path=userpass \
	userpass

vault write auth/userpass/users/$USERNAME password="$PASSWORD"

accessor_id=$(vault auth list -format=json | jq -r '.["userpass/"].accessor')

entity_id=$(vault write -format=json identity/entity \
	name="$ENTITY_NAME" \
	| jq -r ".data.id" \
)

vault write identity/entity-alias \
	name="$USERNAME" \
	canonical_id=$entity_id \
	mount_accessor=$accessor_id

vault write identity/group \
	name="admins" \
	policies="admin" \
	policies="base" \
	member_entity_ids=$entity_id

#
# Return encrypted data
#

#"encrypted_password":$(echo "$encrypted_key_root" | jq -aRs .),
cat >&3 <<EOF
{}
EOF
