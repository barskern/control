# Homecloud

The puppet configuration files for setting up my personal homelab
infrastructure. The main components are [Nomad], [Vault] and [Consul] from the
[HashiCorp] suite.

The goal of the infrastructure is to provide a platform where
services can be as easy to setup as a simple nomad configuration file where
dynamic certificates, dynamic secrets, service discovery and lifecycle
management is easily integrated.

[Nomad]: https://www.nomadproject.io/
[Vault]: https://www.vaultproject.io/
[Consul]: https://www.consul.io/
[HashiCorp]: https://www.hashicorp.com/

## Bootstrapping

To bootstrap the infrastructure firstly one needs a server which is going to act
as the master puppet server, or simply the golden cow. All that is needed is ssh
access, preferable through ssh-keys, to start the setup. Then it's as easy as
following the steps below:

1. Ensure you have ssh access as the root user (e.g. `ssh <server_addr> whoami`
   should return root).
2. Run stage 0 of the bootstrap script:

   ```sh
   bolt plan run control::bootstrap_master::stage0 --targets <server_addr>
   ```

   This will install the required software on the server and start the service
   we need to configure first, namely vault.
3. Modify the variables in `./init-vault`-script and run it while piping the
   output to `eval`:

   ```sh
   eval $(./init-vault)
   ```

   This step will initialize vault and encrypt the master key using the provided
   GPG key. Further it will initialize the current shell session with the
   `VAULT_ADDR` and `VAULT_TOKEN` environment variables which enable running
   `vault` commands directly. This can be verified by running `vault status`.

   *Note: This has to be done outside of the bolt plan due to needing to decrypt
   both the vault key and root token.*
4. Run stage 1 of the bootstrap script:

   ```sh
   bolt plan run control::bootstrap_master::stage1 --targets <server_addr> \
       token=$VAULT_TOKEN \
       gpg_key="$(gpg --export --armor <your-email>)"
   ```

   This will setup a PKI infrastructure in vault, including a CA root and several
   intermediate CA's used for services. The relevant certificates will
   automatically be added to puppet. Further the only two relevant private keys
   (root and infra) will be encrypted and downloaded to the project directory.

## Re-initialize after extended downtime

As vault is configured to use TLS certificates from its own PKI infrastructure
for authentication, extended downtime may cause Vault certificates to be
expired. When this happens, other services cannot authenticate to the Vault API,
and hence the certificates cannot be regenerated. In this case, perform the
following:

```sh
# Ensure Vault is unsealed
export VAULT_KEY=$(cat vault_key.gpg | base64 -d | gpg -dq)
bolt task run control::unseal_vault -t <server_addr> key=$VAULT_KEY

# Manually regenerate the Vault certificates
export VAULT_TOKEN=$(cat vault_root_token.gpg | base64 -d | gpg -dq)
bolt task run control::regen_vault_certs -t <server_addr> token=$VAULT_TOKEN

# Ensure nomad has a valid vault token
bolt task run control::update_nomad_vault_token -t <server_addr> token=$VAULT_TOKEN
```

## Some notes

The reason `vault_lookup` doesn't work with TLS certs, is that the client cert
will not be sent to 3rd parties. See
https://tickets.puppetlabs.com/browse/PUP-11522 for more info.

TODO make a puppet task for registering vault plugins.
TODO make a puppet task for setting up database backend and roles for vault.

Setup key-value secrets engine

```sh
vault secrets enable \
	-version=2 \
	-path=secret \
	-description="key-value secrets" \
	kv
```

Commands to enable 1Password vault plugin. The `op-connect-config.json` should
be a JSON object with two fields, `op_connect_host` and `op_connect_token`.

```sh
#vault write sys/plugins/catalog/secret/op-connect \
#	sha256=7ce3e44c1a5f17e5f9b8dafd7a4c604b86ea862a69e5d2f12684cb0f136f5ba2 \
#	command=vault-plugin-secrets-onepassword_v1.0.0
vault plugin register \
	-sha256=7ce3e44c1a5f17e5f9b8dafd7a4c604b86ea862a69e5d2f12684cb0f136f5ba2 \
	-command=vault-plugin-secrets-onepassword_v1.0.0 \
	-version=v1.0.0 \
	secret \
	op-connect
vault secrets enable \
  -plugin-name=op-connect \
  -path=op \
  -description="read and write to onepassword vaults" \
  plugin
vault write op/config @op-connect-config.json
vault kv put secret/op_connect/credentials @1password-credentials.json
```

Commands to enable password generation plugin.

```sh
#vault write sys/plugins/catalog/secret/secrets-gen \
#	sha256=c6c43e4ffb41d414020be0490dc16700f05df14fea96fbfd9856f7c667e35503 \
#	command=vault-secrets-gen_v0.1.3
vault plugin register \
	-sha256=c6c43e4ffb41d414020be0490dc16700f05df14fea96fbfd9856f7c667e35503 \
	-command=vault-secrets-gen_v0.1.3 \
	-version=v0.1.3 \
	secret \
	secrets-gen
vault secrets enable \
  -path=gen \
  -plugin-name=secrets-gen \
  -description="generate cryptographically secure passwords" \
  plugin
```
