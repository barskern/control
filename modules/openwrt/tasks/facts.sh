#!/bin/sh
#
# Based on the facts script from puppet, but adapted for OpenWrt
#

# This script may be called outside of a task, e.g. by puppet_agent
# so we have to just paste this code here.  *grumbles*
# Exit with an error message and error code, defaulting to 1
fail() {
  # Print a message: entry if there were anything printed to stderr
  if [[ -s $_tmp ]]; then
    # Hack to try and output valid json by replacing newlines with spaces.
    error_data="{ \"msg\": \"$(tr '\n' ' ' <"$_tmp")\", \"kind\": \"sh-error\", \"details\": {} }"
  else
    error_data="{ \"msg\": \"Task error\", \"kind\": \"sh-error\", \"details\": {} }"
  fi
  echo "{ \"status\": \"failure\", \"_error\": $error_data }"
  exit "${2:-1}"
}

validation_error() {
  error_data="{ \"msg\": \""$1"\", \"kind\": \"sh-error\", \"details\": {} }"
  echo "{ \"status\": \"failure\", \"_error\": $error_data }"
  exit 255
}

success() {
  echo "$1"
}

# Get info from one of /etc/os-release or /usr/lib/os-release
# This is the preferred method and is checked first
_parse_osrelease() {
  # These files may have unquoted spaces in the "pretty" fields even if the spec says otherwise
  # source cannot use process subsitution in some versions of bash, so redirect to stdin instead

  # Export all variables in file we source
  set -a
  source "$1"
  set +a


  # According to `man os-release`, the first entry in ID_LIKE
  # should be the one the platform most closely resembles
  if [[ $ID = 'openwrt' ]]; then
    family='OpenWrt'
  elif [[ $ID_LIKE ]]; then
    family="${ID_LIKE%% *}"
  else
    family="${ID}"
  fi
}

# Last resort
_uname() {
  [[ $ID ]] || ID="$(uname)"
  [[ $VERSION_ID ]] || VERSION_ID="$(uname -r)"
}

#_tmp="$(mktemp facts-openwrt-XXXXX.log)"
#exec 2>>"$_tmp"

# Use indirection to munge PT_ environment variables
# e.g. "$PT_version" becomes "$version"
#for v in ${!PT_*}; do
#  declare "${v#*PT_}"="${!v}"
#done

if [[ -e /etc/os-release ]]; then
  _parse_osrelease /etc/os-release
elif [[ -e /usr/lib/os-release ]]; then
  _parse_osrelease /usr/lib/os-release
fi

# If either systemd is not installed or we didn't get a minor version or codename from os-release
if ! [[ $VERSION_ID ]]; then
  _uname
fi

full="${VERSION_ID}"
major="${VERSION_ID%%.*}"
minor="$(echo $VERSION_ID | sed 's/^[0-9]\+\.\([0-9]\+\)\.[0-9]\+/\1/')"

success "$(cat <<EOF
{
  "identity": {
    "user": "$(id -nu)",
    "uid": "$(id -u)",
    "group": "$(id -ng)",
    "gid": "$(id -g)"
  },
  "is_virtual": false,
  "os": {
    "name": "$ID",
    "release": {
      "full": "$full",
      "major": "$major",
      "minor": "$minor"
    },
    "family": "$family"
  },
  "system_uptime": {
    "uptime": "$(uptime)"
  }
}
EOF
)"
