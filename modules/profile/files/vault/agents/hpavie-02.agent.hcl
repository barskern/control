pid_file = "./pidfile"

vault {
  address = "https://vault.service.ruud.cloud:8200"
  ca_cert = "/etc/vault/ca.crt"
  client_cert = "/etc/puppetlabs/puppet/ssl/certs/hpavie-02.home.iruud.cloud.pem"
  client_key = "/etc/puppetlabs/puppet/ssl/private_keys/hpavie-02.home.iruud.cloud.pem"
}

listener "tcp" {
  address = "127.0.0.1:8100"
  tls_disable = true
}

auto_auth {
  method "cert" {}
}

cache {
  use_auto_auth_token = true
}
