pid_file = "./pidfile"

vault {
  address = "https://vault.service.ruud.cloud:8200"
  ca_cert = "/etc/vault/ca.crt"
  client_cert = "/etc/puppetlabs/puppet/ssl/certs/servie.home.iruud.cloud.pem"
  client_key = "/etc/puppetlabs/puppet/ssl/private_keys/servie.home.iruud.cloud.pem"
}

listener "tcp" {
  address = "127.0.0.1:8100"
  tls_disable = true
}

auto_auth {
  method "cert" {}
}

cache {
  use_auto_auth_token = true
}

template {
  command = "systemctl reload vault"
  error_on_missing_key = true
  destination = "/etc/vault/ca.crt"

  contents = <<EOF
{{ with secret "pki_web/issue/ruud_cloud" "common_name=vault.service.ruud.cloud" "alt_names=servie.home.iruud.cloud" "ttl=24h" }}
{{- .Data.issuing_ca -}}
{{ end }}
EOF
}

template {
  command = "systemctl reload vault"
  error_on_missing_key = true
  destination = "/etc/vault/cert.crt"

  contents = <<EOF
{{ with secret "pki_web/issue/ruud_cloud" "common_name=vault.service.ruud.cloud" "alt_names=servie.home.iruud.cloud" "ttl=24h" }}
{{- .Data.certificate }}
{{ range .Data.ca_chain }}{{ . }}
{{ end }}{{ end }}
EOF
}

template {
  command = "systemctl reload vault"
  error_on_missing_key = true
  destination = "/etc/vault/key.pem"
  contents = <<EOF
{{ with secret "pki_web/issue/ruud_cloud" "common_name=vault.service.ruud.cloud" "alt_names=servie.home.iruud.cloud" "ttl=24h" }}
{{- .Data.private_key }}
{{ end }}
EOF
}

# Leave this here for future use..
#template {
#  command = "chown postgres:postgres /var/lib/pgsql/14/data/ca.crt; systemctl reload postgresql-14"
#  perms = "0640"
#  error_on_missing_key = true
#  destination = "/var/lib/pgsql/14/data/ca.crt"
#  contents = <<EOF
#{{ with secret "pki_web/issue/ruud_cloud" "common_name=postgresql.service.ruud.cloud" "alt_names=servie.home.iruud.cloud" "ip_sans=127.0.0.1,10.0.0.2" "ttl=24h" }}
#{{- .Data.issuing_ca -}}
#{{ end }}
#EOF
#}
#
#template {
#  command = "chown postgres:postgres /var/lib/pgsql/14/data/server.crt; systemctl reload postgresql-14"
#  perms = "0640"
#  error_on_missing_key = true
#  destination = "/var/lib/pgsql/14/data/server.crt"
#  contents = <<EOF
#{{ with secret "pki_web/issue/ruud_cloud" "common_name=postgresql.service.ruud.cloud" "alt_names=servie.home.iruud.cloud" "ip_sans=127.0.0.1,10.0.0.2" "ttl=24h" }}
#{{- .Data.certificate }}
#{{ range .Data.ca_chain }}{{ . }}
#{{ end }}{{ end }}
#EOF
#}
#
#template {
#  command = "chown postgres:postgres /var/lib/pgsql/14/data/server.key; systemctl reload postgresql-14"
#  perms = "0640"
#  error_on_missing_key = true
#  destination = "/var/lib/pgsql/14/data/server.key"
#  contents = <<EOF
#{{ with secret "pki_web/issue/ruud_cloud" "common_name=postgresql.service.ruud.cloud" "alt_names=servie.home.iruud.cloud" "ip_sans=127.0.0.1,10.0.0.2" "ttl=24h" }}
#{{- .Data.private_key }}
#{{ end }}
#EOF
#}


template {
  # Touch the dynamic config to make traefik pickup new certs
  command = "touch /etc/traefik/dynamic.toml"
  error_on_missing_key = true
  destination = "/etc/traefik/ruud_cloud.cert"

  contents = <<EOF
{{ with secret "pki_web/issue/ruud_cloud" "common_name=*.service.ruud.cloud" "ttl=12h" }}
{{- .Data.certificate }}
{{ range .Data.ca_chain }}{{ . }}
{{ end }}{{ end }}
EOF
}

template {
  # Touch the dynamic config to make traefik pickup new certs
  command = "touch /etc/traefik/dynamic.toml"
  error_on_missing_key = true
  destination = "/etc/traefik/ruud_cloud.key"

  contents = <<EOF
{{ with secret "pki_web/issue/ruud_cloud" "common_name=*.service.ruud.cloud" "ttl=12h" }}
{{- .Data.private_key -}}
{{ end }}
EOF
}


template {
  # Send the updated CRL to puppet
  command = "curl --silent --write-out ' (status_code=%{http_code})\n' --data-binary @/etc/puppetlabs/puppet/ssl/crl_vault.pem --request PUT --cert /etc/puppetlabs/puppet/ssl/certs/servie.home.iruud.cloud.pem --key /etc/puppetlabs/puppet/ssl/private_keys/servie.home.iruud.cloud.pem --cacert /etc/puppetlabs/puppetserver/ca/ca_crt.pem https://servie.home.iruud.cloud:8140/puppet-ca/v1/certificate_revocation_list"
  error_on_missing_key = true
  destination = "/etc/puppetlabs/puppet/ssl/crl_vault.pem"

  contents = <<EOF
{{ with secret "pki_puppet/cert/crl" }}
{{- .Data.certificate -}}
{{ end }}
{{ with secret "pki_infra/cert/crl" }}
{{- .Data.certificate -}}
{{ end }}
{{ with secret "pki_root/cert/crl" }}
{{- .Data.certificate -}}{{ end }}
EOF
}
