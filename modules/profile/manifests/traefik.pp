# profile::traefik
class profile::traefik (
  Integer $metrics_port = 8082,
) {

  # This means that the puppet playbook depends on onepassword, which is fragile..

  #$traefik_admin_user = Deferred('vault_lookup::lookup', [
  #  '/op/vaults/homelab/items/traefik', {
  #  'vault_addr'  => 'http://127.0.0.1:8100',
  #  'auth_method' => 'agent',
  #}])

  #$variables = {
  #  'admins' => Deferred('new', [Array, [$traefik_admin_user]]),
  #  'users'  => [],
  #}

  #file { '/etc/traefik/dynamic.toml':
  #  ensure => present,
  #  mode   => '0644',
  #  content => stdlib::deferrable_epp('profile/traefik/dynamic.toml.epp', $variables),
  #}

  file { '/etc/traefik/config.toml':
    ensure => present,
    mode   => '0644',
    source => 'puppet:///modules/profile/traefik/config.toml',
  }

  class { '::traefik':
    version          => '3.3.2',
    config_file_path => '/etc/traefik/config.toml',
  }

  firewall { '100 traefik exporter':
    proto  => tcp,
    jump   => accept,
    dport  => $metrics_port,
  }
  consul::service { 'traefik-exporter':
    tags    => ['metrics-prometheus', 'http'],
    address => $facts['networking']['ip'],
    port    => $metrics_port,
    meta    => {
      'metrics-for' => 'traefik',
    },
    checks  => [
      {
        name     => "Traefik TCP connection on ${metrics_port}",
        tcp      => "${::facts['networking']['ip']}:${metrics_port}",
        interval => '10s',
        timeout  => '4s',
      }
    ],
  }

  firewall { '100 traefik http':
    proto  => tcp,
    jump   => accept,
    dport  => 80,
  }
  consul::service { 'traefik-http':
    service_name => 'traefik',
    tags         => ['http', 'web'],
    address      => $facts['networking']['ip'],
    port         => 80,
    checks       => [
      {
        name     => 'Traefik TCP connection on 80',
        tcp      => "${::facts['networking']['ip']}:80",
        interval => '10s',
        timeout  => '4s',
      }
    ],
  }

  firewall { '100 traefik https':
    proto  => tcp,
    jump   => accept,
    dport  => 443,
  }
  firewall { '100 traefik ping':
    proto  => tcp,
    jump   => accept,
    dport  => 8080,
  }
  consul::service { 'traefik':
    service_name => 'traefik',
    tags         => ['https', 'web'],
    address      => $facts['networking']['ip'],
    port         => 443,
    checks       => [
      {
        name            => 'Traefik HTTP ping on 8080',
        http            => "http://${::facts['networking']['ip']}:8080/ping",
        tls_skip_verify => true,
        interval        => '10s',
        timeout         => '4s',
      },
      {
        name     => 'Traefik TCP connection on 443',
        tcp      => "${::facts['networking']['ip']}:443",
        interval => '10s',
        timeout  => '4s',
      }
    ],
  }

  # TODO don't run traefik as root user.
  # NB! When changed, vault-agent should also be changed.
}
