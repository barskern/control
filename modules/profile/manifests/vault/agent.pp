# profile::vault::agent
# Setup and run Vault agent with the specified agent file.
class profile::vault::agent {
  include profile::vault::common

  $config_file = '/etc/vault-agent/config.hcl'
  file { '/etc/vault-agent':
    ensure => 'directory',
  }
  -> file { $config_file:
    ensure => present,
    source => "puppet:///modules/profile/vault/agents/${::facts['hostname']}.agent.hcl",
  }

  # TODO run as vault-agent when possible, but hard to write secret files..
  # group { 'vault-agent':
  #   ensure => present,
  # }
  # user { 'vault-agent':
  #   ensure  => present,
  #   groups  => ['puppet', 'traefik'],
  #   require => Group['vault-agent'],
  # }

  $unit_args = {
    'config_file' => $config_file,
    'bin_dir'     => $profile::vault::common::bin_dir,
  }
  systemd::unit_file { 'vault-agent.service':
    content => epp('profile/vault/vault-agent.service.epp', $unit_args),
  }
  ~> service { 'vault-agent':
    ensure    => 'running',
    enable    => true,
    subscribe => File[$config_file],
    require   => Package['vault'],
  }
}
