# Setup and configure consul as server.
class profile::vault::server (
  Array[Data] $plugins,
  Integer     $default_port = 8200,
  Integer     $cluster_port = 8201,
  Integer     $ui_port      = 8202,
) {
  include profile::vault::common

  $plugin_dir = '/etc/vault/plugins'
  if $::facts['bootstrapping'] {
    $listeners = [{
      tcp => {
        address     => "0.0.0.0:$default_port",
        tls_disable => true,
      }
    }]
  } else {
    $listeners = [
      {
        tcp => {
          address     => "127.0.0.1:$default_port",
          tls_disable => true,
        }
      },
      {
        tcp => {
          address => "${::facts['networking']['ip']}:$ui_port",
          tls_disable => true,
          x_forwarded_for_authorized_addrs => [
            # TODO Make a query for servers with "entrypoint" role
            "${::facts['networking']['ip']}",
          ],
        }
      },
      {
        tcp => {
          address                                     => "${::facts['networking']['ip']}:$default_port",
          #tls_client_ca_file => '/etc/vault/ca.crt',
          #tls_require_and_verify_client_cert => true,
          tls_cert_file                               => '/etc/vault/cert.crt',
          tls_key_file                                => '/etc/vault/key.pem',
          x_forwarded_for_authorized_addrs            => [
            # TODO Make a query for servers with "entrypoint" role
            "${::facts['networking']['ip']}",
          ],
          x_forwarded_for_client_cert_header          => "X-Forwarded-Tls-Client-Cert",
          x_forwarded_for_client_cert_header_decoders => "BASE64",
          x_forwarded_for_reject_not_authorized       => "false",
          x_forwarded_for_reject_not_present          => "false",
        }
      }
    ]
  }

  class { '::vault':
    manage_package   => false,
    bin_dir          => $profile::vault::common::bin_dir,
    enable_ui        => true,
    api_addr         => "https://${::facts['networking']['ip']}:$default_port",
    extra_config     => {
      cluster_addr     => "https://${::facts['networking']['ip']}:$cluster_port",
      plugin_directory => $plugin_dir,
      service_registration => {
        consul => {
          service_tags => join([
            "https",
            "web",
            "traefik.enable=true",
            "traefik.http.middlewares.vault-tlsclient.passtlsclientcert.pem=true",
            "traefik.http.routers.vault.middlewares=vault-tlsclient@consulcatalog",
            "traefik.http.routers.vault.rule=Host(`vault.service.ruud.cloud`)",
            "traefik.http.routers.vault.entryPoints=websecure",
            "traefik.http.routers.vault.service=vault",
            "traefik.http.services.vault.loadbalancer.server.scheme=https",
            "traefik.http.services.vault.loadBalancer.serversTransport=insecure-transport@file",
          ], ','),
        }
      }
    },
    storage          => {
      consul => {},
    },
    telemetry        => {
      disable_hostname          => true,
      prometheus_retention_time => '24h',
    },
    listener         => $listeners,
    purge_config_dir => false,
  }

  file { $plugin_dir:
    ensure => directory,
    mode   => '0755',
    owner  => 'vault',
    group  => 'vault',
  }

  $plugins.each |Data $plugin| {
    archive { "/tmp/${plugin['name']}.${plugin['extension']}":
      url             => $plugin['url'],
      checksum        => $plugin['checksum'],
      checksum_type   => $plugin['checksum_type'],
      extract         => true,
      extract_path    => $plugin_dir,
      # This command ONLY extracts executable to the plugin folder
      extract_command => "unzip -j %s ${plugin['name']}",
      cleanup         => true,
      creates         => "${plugin_dir}/${plugin['name']}",
    } ~> file { "${plugin_dir}/${plugin['name']}":
      mode  => '0755',
      owner => 'root',
      group => 'root',
    } ~> file_capability { "${plugin_dir}/${plugin['name']}":
      capability => 'cap_ipc_lock=ep',
    }
  }

  consul::service { 'vault-ui':
    service_name => 'vault-ui',
    tags         => [
      'https',
      'web',
      "traefik.enable=true",
      "traefik.http.routers.vault-ui.rule=Host(`vault-ui.service.ruud.cloud`)",
      "traefik.http.routers.vault-ui.entryPoints=websecure",
    ],
    address      => $facts['networking']['ip'],
    port         => $ui_port,
    checks       => [
      {
        name     => "Vault TCP connection on $ui_port",
        tcp      => "${::facts['networking']['ip']}:$ui_port",
        interval => '10s',
        timeout  => '4s',
      }
    ],
  }

  firewall { '100 vault server':
    proto  => tcp,
    jump   => accept,
    dport  => 8200,
  }
}
