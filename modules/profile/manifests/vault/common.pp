class profile::vault::common (
  String                $version,
  Stdlib::Absolutepath  $bin_dir = '/usr/bin'
) {
  # TODO Will this always work? A way to get path from package?
  $vault_binary = "/bin/vault"
  package { 'vault':
    ensure  => $version,
    require => Class['hashi_stack::Repo'],
    notify  => [
      Service['vault'],
      Service['vault-agent'],
    ]
  } ~>
  file_capability { 'vault_binary_capability':
    ensure     => present,
    file       => $vault_binary,
    capability => 'cap_ipc_lock=ep',
  }
}
