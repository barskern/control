# Base profile for all puppet nodes
class profile::base (
  Boolean       $ssh_enable = false,
  Array[String] $ssh_keys = [],
) {
  include ::motd
  include ::firewall

  include profile::node_exporter

  ensure_packages(
    ['jq', 'rsync', 'vim'],
    { ensure => 'present' }
  )

  if $facts['os']['release']['major'] == '7' {
    # Only for CentOS 7
    include ::ntp
  }

  if $facts['os']['family'] == 'RedHat' {
    include ::yum
  }

  class { '::ca_cert':
    purge_unmanaged_CAs => true,
  }

  # TODO Might be needed by hiera_vault
  # package { 'vault-puppetpath-gem':
  #   ensure   => 'present',
  #   name     => 'vault',
  #   provider => 'puppet_gem',
  # }
  # package { 'debouncer-puppetpath-gem':
  #   ensure   => 'present',
  #   name     => 'debouncer',
  #   provider => 'puppet_gem',
  # }

  user { 'root':
    ensure         => present,
    purge_ssh_keys => true,
    home           => '/root',
    system         => true,
  }

  if $ssh_enable {
    firewall { '100 ssh access':
      dport  => 22,
      proto  => tcp,
      jump   => accept,
    }

    $ssh_keys.each |String $ssh_key| {
      # First part of key is type, second is the key and third is the comment
      $parts = split($ssh_key, / /)
      ssh_authorized_key { $parts[2]:
        ensure => 'present',
        user   => 'root',
        type   => $parts[0],
        key    => $parts[1],
      }
    }
  }
}
