# Setup and configure consul as either client or server.
class profile::consul (
  Enum['server', 'client']      $role,
  Optional[Stdlib::IP::Address] $master_ip = undef,
) {
  $base_config = {
    node_name   => $::trusted['hostname'],
    datacenter  => 'home',
    domain      => 'ruud.cloud',
    log_level   => 'WARN',
    data_dir    => '/opt/consul',
    ui          => true,
    client_addr => '0.0.0.0',
    bind_addr   => $::facts['networking']['ip'],
    # TODO Set encrypt for consul with vault secret
    #encrypt    => '',
  }

  # Merge configs for all roles into single hash
  $config = $base_config + case $role {
    'server': {{
      bootstrap_expect => 1,
      server           => true,
    }}
    'client': {{
      retry_join => [$master_ip],
    }}
    default: {
      warn("ignoring invalid role '${$role}' passed to ::profile::consul")
    }
  }

  package { 'unzip':
    ensure => present,
  }

  class { '::consul':
    version     => '1.20.2',
    config_hash => $config,
    require     => Package['unzip'],
  }

  include profile::consul_exporter

  $http_port = 8500

  # Ports needed to run consul on the host
  firewall { '100 consul http':
    proto  => tcp,
    jump   => accept,
    dport  => $http_port,
  }
  consul::service { 'consul-ui':
    service_name => 'consul-ui',
    tags         => [
      "http",
      "web",
      "traefik.enable=true",
      "traefik.http.routers.consul-ui.rule=Host(`consul-ui.service.ruud.cloud`)",
      "traefik.http.routers.consul-ui.entryPoints=websecure",
    ],
    address      => $facts['networking']['ip'],
    port         => $http_port,
    checks       => [
      {
        name     => "Consul TCP connection on $http_port",
        tcp      => "${::facts['networking']['ip']}:$http_port",
        interval => '10s',
        timeout  => '4s',
      }
    ],
  }

  firewall { '100 consul rpc':
    proto  => tcp,
    jump   => accept,
    dport  => 8300,
  }
  ['udp', 'tcp'].each |$proto| {
    firewall { "100 consul dns ${proto}":
      proto  => $proto,
      jump   => accept,
      dport  => 8600,
    }
    firewall { "100 consul serf_lan ${proto}":
      proto  => $proto,
      jump   => accept,
      dport  => 8301,
    }
    firewall { "100 consul serf_wan ${proto}":
      proto  => $proto,
      jump   => accept,
      dport  => 8302,
    }
  }
}
