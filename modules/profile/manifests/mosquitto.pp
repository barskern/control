class profile::mosquitto {

  class { '::mosquitto':
    config => [
      'allow_anonymous true',
      'listener 1883',
      'listener 9001',
      'protocol websockets',
    ],
  }

  firewall { '100 mosquitto mqtt':
    proto  => tcp,
    jump   => accept,
    dport  => 1883,
  }
  firewall { '100 mosquitto mqtt websocket':
    proto  => tcp,
    jump   => accept,
    dport  => 9001,
  }

  consul::service { 'mosquitto-websocket':
    service_name => 'mosquitto-ws',
    tags         => ['websocket'],
    address      => $facts['networking']['ip'],
    port         => 9001,
    checks       => [
      {
        name     => 'Mosquitto TCP connection to websocket on 9001',
        tcp      => "${::facts['networking']['ip']}:9001",
        interval => '10s',
        timeout  => '4s',
      }
    ],
  }

  consul::service { 'mosquitto-tcp':
    service_name => 'mosquitto',
    tags         => ['tcp'],
    address      => $facts['networking']['ip'],
    port         => 1883,
    checks       => [
      {
        name     => 'Mosquitto TCP connection on 1883',
        tcp      => "${::facts['networking']['ip']}:1883",
        interval => '10s',
        timeout  => '4s',
      }
    ],
  }
}
