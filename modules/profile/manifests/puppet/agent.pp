# The configuration for being a ONLY a puppet agent
class profile::puppet::agent (
  String $puppet_server = $server_facts['servername'],
) {
  $defaults = {
    path   => '/etc/puppetlabs/puppet/puppet.conf',
    notify => [Service['puppet']],
  }
  $config = {
    main  => {
      server               => $puppet_server,
      crl_refresh_interval => '1d',
      preprocess_deferred  => true,
    }
  }
  inifile::create_ini_settings($config, $defaults)

  # TODO enable when suitable
  $puppet_ensure = $::facts['bootstrapping'] ? { true => 'stopped', default => 'running' }

  service { 'puppet':
    #ensure => $puppet_ensure,
    ensure => 'stopped',
  }
}
