# The configuration for being a puppet master
class profile::puppet::master {
  class { 'r10k':
    remote => 'https://gitlab.com/barskern/control.git',
  }

  package { ['puppetserver', 'git']:
    ensure => latest,
  }

  # Needed by hiera_vault lookup
  package { 'vault-puppetpath-gem':
    ensure   => 'present',
    name     => 'vault',
    provider => 'puppet_gem',
  }
  -> package { 'debouncer-puppetpath-gem':
    ensure   => 'present',
    name     => 'debouncer',
    provider => 'puppet_gem',
  }
  -> package { 'vault-puppetserver-gem':
    ensure   => 'present',
    name     => 'vault',
    provider => 'puppetserver_gem',
  }
  -> package { 'debouncer-puppetserver-gem':
    ensure   => 'present',
    name     => 'debouncer',
    provider => 'puppetserver_gem',
  }
  ~> Service['puppetserver']

  $defaults = {
    path   => '/etc/puppetlabs/puppet/puppet.conf',
    notify => [Service['puppetserver'], Service['puppet']],
  }
  $config = {
    master => {
      vardir => '/opt/puppetlabs/server/data/puppetserver',
      logdir => '/var/log/puppetlabs/puppetserver',
      rundir => '/var/run/puppetlabs/puppetserver',
      pidfile => '/var/run/puppetlabs/puppetserver/puppetserver.pid',
      codedir => '/etc/puppetlabs/code',
    },
    main => {
      dns_alt_names => [],
      crl_refresh_interval => '1d',
    },
    agent  => {
      server      => $::facts['networking']['fqdn'],
      runinterval => 300,
    }
  }
  inifile::create_ini_settings($config, $defaults)

  $puppet_ensure = $::facts['bootstrapping'] ? { true => 'stopped', default => 'running' }

  service { 'puppetserver':
    ensure => $puppet_ensure,
  }

  firewall { '100 puppetserver':
    proto  => tcp,
    jump   => accept,
    dport  => 8140,
  }

  service { 'puppet':
    #ensure => $puppet_ensure,
    ensure => 'stopped',
  }
}
