# A profile to setup node exporter
class profile::node_exporter (
  Integer $port = 9100
) {

  class { '::prometheus::node_exporter':
    scrape_port => $port,
  }

  firewall { '120 node_exporter':
    proto  => tcp,
    dport  => $port,
    jump   => 'accept',
  }
  consul::service { 'node-exporter':
    service_name => 'node-exporter',
    tags         => ['metrics-prometheus', 'http'],
    address      => $::facts['networking']['ip'],
    port         => $port,
    meta         => {
      'machine' => $::trusted['hostname'],
    }
  }
}

