# Profile to setup simple git server
class profile::git_server {
  group { 'data':
    ensure => present,
  }

  file { '/data':
    ensure => 'directory',
    group  => 'data',
    mode   => '0770',
  }

  file { '/data/git':
    ensure  => 'directory',
    group   => 'git',
    owner   => 'git',
    mode    => '0755',
    seltype => 'user_home_t',
  }

  file { '/data/git/.ssh':
    ensure  => 'directory',
    group   => 'git',
    owner   => 'git',
    mode    => '0700',
    seltype => 'ssh_home_t',
  }

  user { 'git':
    ensure         => present,
    groups         => ['data'],
    home           => '/data/git',
    purge_ssh_keys => true,
    shell          => '/usr/bin/git-shell',
  }

  ssh_authorized_key { 'oruud@bitfenix':
    ensure  => present,
    user    => 'git',
    type    => 'ed25519',
    key     => 'AAAAC3NzaC1lZDI1NTE5AAAAIG2SH5bHusHAH6WbnI953g5qifKTWpbBwxbflJ3QCKo0',
    options => [
      'no-port-forwarding',
      'no-X11-forwarding',
      'no-agent-forwarding',
      'no-pty',
    ],
  }

  package { 'git':
    ensure => present,
  }
}
