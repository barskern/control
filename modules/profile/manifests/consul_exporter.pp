
# A consul exporter definition for cluster nodes
class profile::consul_exporter (
  Integer $port = 9107,
) {

  class { '::prometheus::consul_exporter':
    scrape_port   => $port,
    # There is a consul agent on every node
    consul_server => '127.0.0.1:8500',
  }

  firewall { '121 consul_exporter':
    proto  => tcp,
    jump   => accept,
    dport  => $port,
  }
  consul::service { 'consul-exporter':
    service_name => 'consul-exporter',
    tags         => ['metrics-prometheus', 'http'],
    address      => $::facts['networking']['ip'],
    port         => $port,
    meta         => {
      'metrics_for' => 'consul',
      'machine'     => $::trusted['hostname'],
    }
  }
}
