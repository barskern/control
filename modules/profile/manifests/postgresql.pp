# Profile to setup a postgres instance with default configuration
class profile::postgresql (
  Sensitive[String]           $admin_password    = Sensitive.new('admin'),
  Array[Hash[String,Any]]     $databases_to_make = [],
  Integer                     $metrics_port      = 9187,
  String[1]                   $major_version     = '16',
) {

  # (Maybe not) needed for PostgreSQL ${major_version} on CentOS 7
  #yum::install { 'pgdg-redhat-repo':
  #  ensure   => 'present',
  #  source   => 'https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm',
  #}

  # TODO Is this hardcoded for CentOS or...?
  $bindir = "/usr/pgsql-${major_version}/bin"

  class { '::postgresql::globals':
    manage_package_repo => true,
    version             => $major_version,
  }

  package { "postgresql${major_version}-contrib":
    ensure  => 'present',
    notify  => Service["postgresql-${major_version}"]
  }

  package { "timescaledb-2-postgresql-${major_version}":
    ensure  => 'present',
    require => Yumrepo['timescale_timescaledb'],
  } ~> exec { 'timescaledb-tune':
    command     => "timescaledb-tune -yes -pg-config ${bindir}/pg_config",
    path        => ['/usr/bin', '/usr/sbin'],
    refreshonly => true,
    notify      => Service["postgresql-${major_version}"]
  }


  # Fetch and unpack config for postgres from 1password
  #$admin_password_vault = Deferred('vault_lookup::lookup', [
  #  '/op/vaults/homelab/items/postgresql', {
  #  'vault_addr'  => 'http://127.0.0.1:8100',
  #  'auth_method' => 'agent',
  #  'field'       => 'password',
  #}])

  class { '::postgresql::client': }
  class { '::postgresql::server':
    # TODO Soon! Only missing that Postgres puppet model supports deferred values.
    #postgres_password           => Deferred('unwrap', [$admin_password_vault]),
    postgres_password           => $admin_password,
    listen_addresses            => "127.0.0.1,${::facts['networking']['ip']}",
    #listen_addresses           => "0.0.0.0",
    #ip_mask_deny_postgres_user => '0.0.0.0/32',
    #ip_mask_allow_all_users    => '0.0.0.0/0',
    config_entries              => {
      "timescaledb.telemetry_level" => "off",
      "shared_preload_libraries"    => "timescaledb",
    },
  }

  class { '::prometheus::postgres_exporter':
    version         => '0.15.0',
    # TODO make a separate role for monitoring that does this!
    data_source_uri => 'localhost:5432/postgres?sslmode=disable',
    postgres_user   => 'postgres',
    postgres_pass   => $admin_password.unwrap,
    scrape_port     => $metrics_port,
  }

  $databases_to_make.each |$database| {
    $database_name = $database['name']
    $database_exts = $database['extensions']

    $username = "vault_${database_name}"

    postgresql::server::role { $username:
      # We don't want this as the password as it is changed by vault itself
      # after creation
      #update_password => true,

      password_hash => postgresql::postgresql_password($username, 'vault'),
      # Is this really needed? Let's leave it on for now
      superuser     => true,
      createdb      => true,
      createrole    => true,
      hash          => 'scram-sha-256',
    } ~> postgresql::server::database { $database_name:
      owner => $username,
    } ~> postgresql::server::database_grant { "GRANT ${username} - ALL - ${database_name}":
      privilege => 'ALL',
      db        => $database_name,
      role      => $username,
    } ~> postgresql::server::pg_hba_rule { "allow all users to access ${database_name} on local network":
      type        => 'host',
      database    => $database_name,
      # Username will later be dynamic when generaed by vault,
      # so we need to allow all users
      user        => 'all',
      # TODO make this a parameter or something else smart
      address     => '10.0.0.0/8',
      auth_method => 'md5',
    }

    if $database_exts =~ Array[String]  {
      $database_exts.each |$extension_name| {
        postgresql::server::extension { $extension_name:
          database => $database_name,
        }
      }
    }
  }

  file { "/etc/profile.d/postgresql-helpers-${major_version}.sh":
    ensure  => file,
    content => epp("profile/psql-helpers.sh.epp", {
      'bindir'    => $bindir,
      'databases' => $databases_to_make,
    }),
  }

  firewall { '100 postgresql':
    proto  => tcp,
    jump   => accept,
    dport  => 5432,
  }
  consul::service { 'postgresql':
    service_name => 'postgresql',
    tags         => ['database'],
    address      => $facts['networking']['ip'],
    port         => 5432,
    checks       => [
      {
        name     => 'PostgreSQL TCP connection on 5432',
        tcp      => "${::facts['networking']['ip']}:5432",
        interval => '10s',
        timeout  => '4s',
      }
    ],
  }

  firewall { '121 postgresql':
    proto  => tcp,
    jump   => accept,
    dport  => $metrics_port,
  }
  consul::service { 'postgresql-exporter':
    service_name => 'postgresql-exporter',
    tags         => ['metrics-prometheus', 'http'],
    address      => $facts['networking']['ip'],
    port         => $metrics_port,
    meta         => {
      'metrics_for' => 'postgres',
    },
  }
}
