# Profile to configure docker
class profile::docker {
  class { '::docker':
    dns => '10.0.0.1',
  }
}
