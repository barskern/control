# Profile for configuring cluster nodes
class profile::cluster_node (
  Array[Enum['server', 'client', 'entrypoint'], 1] $roles,
) {
  if 'client' in $roles {
    include ::profile::docker
  }

  if 'server' in $roles {
    include ::profile::vault::server
    class { '::profile::consul':
      role => 'server',
    }
  } else {
    class { '::profile::consul':
      role => 'client',
    }
  }

  if 'entrypoint' in $roles {
    class { '::profile::traefik': }
  }

  class { '::profile::nomad':
    roles => $roles.filter |$role| { $role != 'entrypoint' },
  }
}
