# Setup and configure nomad as either client, server or both.
class profile::nomad (
  Array[Enum['server', 'client'], 1] $roles,
  Hash                               $meta = {},
  Hash                               $host_volumes = {},
  Hash                               $host_networks = {},
) {
  $base_config = {
    region     => 'eu',
    log_level  => 'WARN',
    datacenter => 'home',
    data_dir   => '/opt/nomad',
    telemetry  => {
      collection_interval => '5s',
      disable_hostname => true,
      prometheus_metrics => true,
      publish_allocation_metrics => true,
      publish_node_metrics => true,
    },
    vault      => {
      enabled          => true,
      cert_file        => "/etc/puppetlabs/puppet/ssl/certs/${::trusted['certname']}.pem",
      key_file         => "/etc/puppetlabs/puppet/ssl/private_keys/${::trusted['certname']}.pem",
      address          => "https://vault.service.ruud.cloud:8200",
      create_from_role => "nomad_cluster",
    },
    ui         => {
      enabled => true,
      consul  => {
        ui_url => "https://consul-ui.service.ruud.cloud/ui"
      },
      vault  => {
        ui_url => "https://vault-ui.service.ruud.cloud/ui"
      },
    },
  }

  # Merge configs for all roles into single hash
  $config = $roles
    .map |$role| {
      case $role {
        'server': {{
          server => {
            enabled          => true,
            bootstrap_expect => 1,
            raft_protocol    => 3,
          },
        }}
        'client': {{
          client => {
            enabled      => true,
            meta         => $meta,
            host_volume  => $host_volumes,
            host_network => $host_networks,
          },
          plugin => {
            docker => {}
          }
        }}
        default: {
          warn("ignoring invalid role '${$role}' passed to ::profile::nomad")
        }
      }
    }
    .reduce($base_config) |$acc, $value| { $acc + $value }

  systemd::dropin_file { 'token-env.conf':
    unit    => 'nomad.service',
    content => "[Service]\nEnvironmentFile=-/etc/vault-agent/token-nomad-env",
    notify  => Service['nomad'],
  }

  class { '::nomad':
    # TODO Prior to moving to 1.9, we have to move to workload-based Vault
    # authentication, whatever that entails..
    version     => '1.8.4',
    config_hash => $config,
  }

  $http_port = 4646
  consul::service { 'nomad-ui':
    service_name => 'nomad-ui',
    tags         => [
      'http',
      'web',
      "traefik.enable=true",
      "traefik.http.routers.nomad-ui.rule=Host(`nomad-ui.service.ruud.cloud`)",
      "traefik.http.routers.nomad-ui.entryPoints=websecure",
    ],
    address      => $facts['networking']['ip'],
    port         => $http_port,
    checks       => [
      {
        name     => "Nomad TCP connection on $http_port",
        tcp      => "${::facts['networking']['ip']}:$http_port",
        interval => '10s',
        timeout  => '4s',
      }
    ],
  }

  # Ports needed to run nomad
  firewall { '100 nomad http':
    dport  => $http_port,
    proto  => tcp,
    jump   => accept,
  }
  firewall { '100 nomad rpc':
    dport  => 4647,
    proto  => tcp,
    jump   => accept,
  }
  ['udp', 'tcp'].each |$proto| {
    firewall { "100 nomad serf ${proto}":
      proto  => $proto,
      dport  => 4648,
      jump   => accept,
    }
  }
}

