# The puppet master server
class role::puppet_master {
  include ::profile::base
  include ::profile::puppet::master
}
