# A cluster node
class role::cluster_node {
  include ::profile::base
  include ::profile::cluster_node
}
