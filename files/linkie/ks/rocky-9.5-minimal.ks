#version=Rocky Linux 9
#documentation: https://docs.fedoraproject.org/en-US/fedora/f36/install-guide/appendixes/Kickstart_Syntax_Reference/

# PRE-INSTALLATION SCRIPT
%pre --interpreter=/usr/bin/bash --log=/root/anaconda-ks-pre.log
%end

# INSTALL USING TEXT MODE
text

# KEYBOARDS, LANGUAGES, TIMEZONE
keyboard --vckeymap=us --xlayouts=us
lang en_US.UTF-8
timezone UTC --utc

# NETWORK, SELINUX, FIREWALL
network --device=link --bootproto=dhcp --onboot=on --noipv6 --activate
selinux --disabled
firewall --enabled --ssh

# DISKS, PARTITIONS, VOLUME GROUPS, LOGICAL VOLUMES
# Install target is usually sda, vda, or nvme0n1; adjust all references below accordingly.
# The EFI & /boot partitions are explicitly set here, but some people just use `reqpart`.
ignoredisk --only-use=sda
zerombr
clearpart --all --initlabel --disklabel=gpt
bootloader --location=mbr --boot-drive=sda --append='crashkernel=auto'

# Autopartition
autopart --type=plain --nohome --fstype=xfs

# INSTALLATION SOURCE, EXTRA REPOSITOROIES, PACKAGE GROUPS, PACKAGES
url --url="http://linkie.home.iruud.cloud/repo/os/x86_64/rocky-9.5/BaseOS/"
repo --name="AppStream" --baseurl="http://linkie.home.iruud.cloud/repo/os/x86_64/rocky-9.5/AppStream/" --cost=10
repo --mirrorlist="http://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=extras-$releasever" --name=Extras --cost=50

# Extras repository is needed to install `epel-release` package.
%packages --retries=5 --timeout=20 --inst-langs=en
@^minimal-environment
epel-release
openssh-server
-iwl*-firmware*
%end

# GROUPS, USERS, ENABLE SSH, FINISH INSTALL
rootpw --lock
user --name=oruud --password='init' --plaintext --gecos='Ole Martin Ruud' --groups='wheel'
sshkey --username=oruud 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGNeo+YbCAKfhcZEEnRddy9QpAMza9rRQtORF6fjnEQm oruud@tuxie'
services --enabled='sshd.service'
reboot --eject

# ENABLE EMERGENCY KERNEL DUMPS FOR DEBUGGING
%addon com_redhat_kdump --reserve-mb=auto --enable
%end

# POST-INSTALLATION SCRIPT
%post --interpreter=/usr/bin/bash --log=/root/anaconda-ks-post.log --erroronfail
# Enable CodeReady Builder repo (requires `epel-release` package).
/usr/bin/dnf config-manager --set-enabled crb
%end

