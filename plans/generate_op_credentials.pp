# @summary A plan to generate credentials in onepassword
# @param targets The targets to run on.
# @param token Vault token to use
# @param login_configs Array with hashes with configuration for the template
# @param database_configs Array with hashes with configuration for the template
plan control::generate_op_credentials(
  TargetSpec $targets,
  Sensitive[String] $token = Sensitive(''),
  Array[Hash[String, Any]] $login_configs,
  Array[Hash[String, Any]] $database_configs,
) {
  $master = get_target($targets)

  $vault_read_result = run_task(
    'http_request',
    $master,
    'fetch existing credentials',
    'base_url'      => 'http://127.0.0.1:8200/v1/op/vaults/homelab/items?list=true',
    'method'        => 'get',
    'json_endpoint' => true,
    'headers'       => {
      'X-Vault-Token' => $token.unwrap,
    },
  )[0]
  if ($vault_read_result['status_code'] != 200) {
    fail("Unable to fetch existing secrets, stopping: ${vault_read_result}")
  }
  $existing_titles = $vault_read_result['body']['data']['keys'].map |$k| { $k.split(' ')[0] }
  out::message("Following credentials already exists, they will not be re-made: ${existing_titles.join(', ')}")

  [
    { 'configs' => $login_configs, 'template_path' => 'control/vault/gen-op-user-login.json.epp' },
    { 'configs' => $database_configs, 'template_path' => 'control/vault/gen-op-database-credentials.json.epp' },
  ].each |$data| {
    $data['configs'].filter |$config| {
      if ($config['title'] in $existing_titles) {
        out::message("'${config['title']}' already exists, skipping..")
        false
      } else {
        true
      }
    }.each |$config| {
      $content = epp($data['template_path'], $config)

      $vault_write_result = run_task(
        'http_request',
        $master,
        "generate credentials for '${config['title']}'",
        # Run on master AFTER port change
        'base_url'      => 'http://127.0.0.1:8200/v1/op/vaults/homelab/items',
        'method'        => 'post',
        'body'          => $content,
        'json_endpoint' => true,
        'headers'       => {
          'X-Vault-Token' => $token.unwrap,
        },
      )[0]
      out::message("Generated credentials for '${config['title']}' with status_code  '${vault_write_result['status_code']}'")
      out::message($vault_write_result)
    }
  }
}
