plan control::bootstrap_agent(
  TargetSpec $targets,
) {
  # Install the puppet-agent package and gather facts
  $targets.apply_prep

  # Apply Puppet code
  $apply_results = apply($targets) {
    class { 'puppet_agent':
      #version => '7.20.0',
      config => [
        #{section => main, setting => runinterval, value => '1h'},
        #{section => main, setting => environment, ensure => absent}
        {section => main, setting => crl_refresh_interval, value => '1d'},
        {section => main, setting => server, value => 'servie.home.iruud.cloud'},
      ]
    }
  }

  # Print log messages from the report
  $apply_results.each |$result| {
    $result.report['logs'].each |$log| {
      out::message("${log['level'].upcase}: ${log['message']}")
    }
  }
}
