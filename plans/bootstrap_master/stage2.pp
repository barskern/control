plan control::bootstrap_master::stage2(
  TargetSpec $targets,
  Sensitive[String] $token = Sensitive(''),
) {
  $master = get_target($targets)
  $master.apply_prep

  out::message('Updating nomad vault token (if expired)..')
  run_task('control::update_nomad_vault_token', $master, 'token' => $token.unwrap)

  out::message('Generating needed credentials in onepassword..')

  $login_configs = lookup('vault_onepassword::credential_login_configs')
  $database_configs = lookup('vault_onepassword::credential_database_configs')

  $login_result = run_plan('control::generate_op_credentials', $master,
    'token'            => $token,
    'login_configs'    => $login_configs,
    'database_configs' => $database_configs,
  )

  out::message($login_result)
}
