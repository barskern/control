plan control::bootstrap_master::stage0(
  TargetSpec $targets,
) {
  $master = get_target($targets)
  add_facts($master, { 'bootstrapping' => true })

  $master.apply_prep

  apply($master) {
    include ::role::cluster_node
    include ::role::puppet_master
  }
}
