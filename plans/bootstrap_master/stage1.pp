plan control::bootstrap_master::stage1(
  TargetSpec $targets,
  Sensitive[String] $token = Sensitive(''),
  String $gpg_key = '',
  Boolean $skip_root = false,
) {
  $master = get_target($targets)
  add_facts($master, { 'bootstrapping' => true })
  $master.apply_prep

  $setup_ca_result = run_task('control::setup_vault_ca', $master,
    'token'   => $token.unwrap,
    'gpg_key' => $gpg_key,
    'cert_addr' => "https://${$master.host}:8200",
    'skip_root' => $skip_root,
  )[0]

  out::message('Writing encrypted private keys to current folder...')
  if (!$skip_root) {
    file::write('key_root.pem.gpg', $setup_ca_result['encrypted_key_root'])
  }
  file::write('key_infra.pem.gpg', $setup_ca_result['encrypted_key_infra'])

  [
    'pki_root',
    'pki_infra',
    'pki_puppet',
    'pki_web',
  ].each |$engine| {
    $name = $engine[4,-1]

    out::message("Fetch CA for ${name}...")
    $vault_cert = run_task('http_request', $master,
      'base_url'      => "http://127.0.0.1:8200/v1/${engine}/cert/ca",
      'method'        => 'get',
      'json_endpoint' => true,
      'headers'       => {
        'X-Vault-Token' => $token.unwrap,
      },
    )[0]

    $file = "modules/profile/files/ruud_cloud_${name}_ca.crt"
    out::message("Writing CA to modules/profile/files at 'ruud_cloud_${name}_ca.crt'...")
    file::write($file, $vault_cert['body']['data']['certificate'])
  }
}
