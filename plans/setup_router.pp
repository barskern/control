plan control::setup_router(
  TargetSpec $targets,
) {
  # Custom openwrt facts
  $result_set = run_task('openwrt::facts', $targets)
  $result_set.each |$result| {
    add_facts($result.target, $result.value)
  }

  get_targets($targets).each |$target| {

    # TODO Fix all hardcoding of linkie name for files etc
    $files_dir = "control/linkie"

    # Fetch needed secrets from vault
    $email_data = vault_lookup::lookup('/op/vaults/homelab/items/email-noreply')
    $domeneshop_api = vault_lookup::lookup('/op/vaults/homelab/items/domeneshop-api')
    $home_wifi = vault_lookup::lookup('/op/vaults/homelab/items/home-wifi')

    $wifi_password = Sensitive.new($home_wifi.unwrap['wireless network password'])

    $wg_ip = '10.99.99.1/24'
    $wg_port = '51820'
    $wg_key = Sensitive.new(file::read("$files_dir/wg/server.key").strip)
    $wg_clients = [
      {
        name        => 'iphone',
        pub_key     => file::read("$files_dir/wg/iphone.pub").strip,
        psk_key     => Sensitive.new(file::read("$files_dir/wg/iphone.psk").strip),
        allowed_ips => [
          '10.99.99.2/32', # Peer address
        ]
      },
      {
        name        => 'tuxie',
        pub_key     => file::read("$files_dir/wg/tuxie.pub").strip,
        psk_key     => Sensitive.new(file::read("$files_dir/wg/tuxie.psk").strip),
        allowed_ips => [
          '10.99.99.3/32', # Peer address
        ]
      }
    ]


    $packages = [
      'kmod-usb-storage',
      'kmod-fs-ntfs',
      'kmod-fs-isofs',
      'losetup',
      'usbutils',
      'block-mount',
      'fdisk',
      'blkid',
      'hdparm',
      'openssh-sftp-server',
      'adblock',
      'msmtp',
      'curl',
      'ca-bundle',
      'ddns-scripts',
      'wireguard-tools',
      'luci-proto-wireguard',
      'prometheus-node-exporter-lua',
      'prometheus-node-exporter-lua-nat_traffic',
      'prometheus-node-exporter-lua-netstat',
      'prometheus-node-exporter-lua-openwrt',
      'prometheus-node-exporter-lua-wifi',
      'prometheus-node-exporter-lua-wifi_stations',
    ]
    run_command('opkg update', $target)
    run_command("opkg install ${join($packages, ' ')}", $target)

    upload_file(
      "$files_dir/authorized_keys",
      '/etc/dropbear/authorized_keys',
      $target,
    )
    #upload_file(
    #  "$files_dir/dnsmasq.conf",
    #  '/etc/dnsmasq.conf',
    #  $target,
    #)

    # Upload plain configuration files
    $config_dir = '/etc/config'
    [
      'uhttpd',
      'adblock',
      'dhcp',
      'dropbear',
      'system',
      'fstab',
      'prometheus-node-exporter-lua',
    ].each |$filename| {
      upload_file(
        "$files_dir/config/$filename",
        "$config_dir/${$filename.basename}",
        $target,
      )
    }

    # Upload tftp files
    $tftp_dir = '/mnt/usb/tftp'
    run_command("mkdir -p $tftp_dir", $target)
    [
      'boot.ipxe',
      'menu.ipxe',
      'rocky.ipxe',
      'fedora.ipxe',
      'ipxe-x86_64.efi',
      'ipxe-i386.efi',
      'undionly.kpxe',
    ].each |$filename| {
      upload_file(
        "$files_dir/tftp/$filename",
        "$tftp_dir/${$filename.basename}",
        $target,
      )
    }

    # Upload kickstart files
    $ks_dir = '/mnt/usb/static/ks'
    run_command("mkdir -p $ks_dir", $target)
    [
      'rocky-9.5-minimal.ks'
    ].each |$filename| {
      upload_file(
        "$files_dir/ks/$filename",
        "$ks_dir/${$filename.basename}",
        $target,
      )
    }

    $aps = [
      {'interface' => 'lan', 'basename' => 'toastmaster', 'password' => $wifi_password},
      {'interface' => 'iot', 'basename' => 'toastiot', 'password' => Sensitive.new('ilikeiot')},
      {'interface' => 'guest', 'basename' => 'toastburnt', 'password' => Sensitive.new('ilikeburns')},
    ]
    $filename_wireless = "$files_dir/config/wireless.epp"
    write_file(
      epp($filename_wireless, {aps => $aps}),
      "$config_dir/${$filename_wireless.basename('.epp')}",
      $target,
    )

    $filename_ddns = "$files_dir/config/ddns.epp"
    $content_ddns = epp($filename_ddns, {
      username => $domeneshop_api.unwrap['username'],
      password => Sensitive.new($domeneshop_api.unwrap['credential']),
      domains  => [
        $target.host,
      ],
    })
    write_file(
      $content_ddns,
      "$config_dir/${$filename_ddns.basename('.epp')}",
      $target,
    )

    $filename_firewall = "$files_dir/config/firewall.epp"
    $content_firewall = epp($filename_firewall, {
      wg_port    => $wg_port,
    })
    write_file(
      $content_firewall,
      "$config_dir/${$filename_firewall.basename('.epp')}",
      $target,
    )

    $filename_network = "$files_dir/config/network.epp"
    $content_network = epp($filename_network, {
      wg_ip      => $wg_ip,
      wg_key     => $wg_key,
      wg_port    => $wg_port,
      wg_clients => $wg_clients,
    })
    write_file(
      $content_network,
      "$config_dir/${$filename_network.basename('.epp')}",
      $target,
    )

    # NB! This goes directly in /etc
    $filename_msmtp = "$files_dir/config/msmtprc.epp"
    $content_msmtp = epp($filename_msmtp, {
      account    => 'noreply',
      smtp_host  => $email_data.unwrap['smtp'].split(':')[0],
      smtp_port  => $email_data.unwrap['smtp'].split(':')[1],
      from_email => $email_data.unwrap['email'],
      username   => $email_data.unwrap['username'],
      password   => Sensitive.new($email_data.unwrap['password']),
    })
    write_file(
      $content_msmtp,
      "/etc/${$filename_msmtp.basename('.epp')}",
      $target,
    )

    # Apply changes by reloading/restarting services
    # /etc/init.d/dropbear reload
    run_command(
      @(END)
        /etc/init.d/uhttpd reload
        /etc/init.d/adblock reload
        /etc/init.d/ddns restart
        /etc/init.d/dnsmasq reload
        /etc/init.d/prometheus-node-exporter-lua reload
        /etc/init.d/fstab reload
        /etc/init.d/firewall reload

        /etc/init.d/network restart
        /etc/init.d/system reload
      | END
      ,
      $target
    )
  }
}
