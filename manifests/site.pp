## site.pp ##

## Active Configurations ##

# Use master server filebucket as backup location
# filebucket { 'main':
#   path => false,
# }
# File { backup => main, }

## Ensure only specified ports are open ##
resources { 'firewall':
  # TODO Figure out how to integrate this with nomad/docker,
  # as docker manages it's own rules.
  purge => false,
}

## Node Definitions ##

node default {
  warning("'${facts['networking']['fqdn']}' at '${facts['networking']['ip']}' is using default node definition")

  notify { 'default':
    message => "'${facts['networking']['fqdn']}' at '${facts['networking']['ip']}' is using default node definition",
  }
}

node /(swiftie|bitfenix).home.iruud.cloud/ {
  # TODO move into role
  include ::profile::base
  include ::profile::puppet::agent
}

node 'servie.home.iruud.cloud' {
  include ::role::puppet_master
  include ::role::cluster_node

  # TODO move into role
  include ::profile::vault::agent
  include ::profile::postgresql
  include ::profile::mosquitto
}

node 'hpavie-02.home.iruud.cloud' {
  include ::role::cluster_node

  # TODO move into role
  include ::profile::vault::agent
  include ::profile::puppet::agent
}
